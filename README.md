cireson-software-asset-reconcile
===================

Allows you to reconcile software items and software assets in Microsoft SCSM

**Summary:**

- This script runs with no parameters!
- It will find any software item in SCSM and then create two arrays
	- One array will have any software item that is already associated with a Cireson Software Asset
	- The other array will have any software item that is *not* already associated with a Cireson Software Asset

**Changelog:**

- Version .05 (June 1, 2015
	- Changed add-member lines to be sane, this should work better :)
- Version .041 (Sept 11, 2014)
	- Fixed script so it could handle a single software item associated with multiple hardware assets. It now will display a list of unique properties for each hardware asset, if multiple are associated.
- Version .04 (Sept 11, 2014)
	- Added information about the related Cireson Software Asset, if one exists. Should aid in troubleshooting what software items are related to what.
- Version .03 (Sept 10, 2014)
	- Added dump to XLSX for both associated and non-associated software items (controllable via variable, enabled by default).
- Version .02 (Sept 10, 2014)
	- Added dump to CSV for both associated and non-associated software items (automatic, not controllable via variable).
- Version .01 (Sept 10, 2014)
	- Initial Version
	- Minimal error checking, but it works!